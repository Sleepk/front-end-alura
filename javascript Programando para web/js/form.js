var botaoAdicionar = document.querySelector("#adicionar-paciente");

// noinspection Annotator
botaoAdicionar.addEventListener("click", function (event) {
    event.preventDefault();

    // pego as informacoes do ID form
    var form = document.querySelector("#form-adiciona");

    var paciente = obtemPacienteDoFormulario(form);
    var erros = validaPaciente(paciente);
    if (erros.length > 0) {
        exibiMensagensDeErro(erros);
        return;
    }

    adicionaPacienteNaTabela(paciente);

    form.reset();

    var mensagensErro = document.querySelector("#mensagens-erro");
    mensagensErro.innerHTML = "";
});


function obtemPacienteDoFormulario(form) {
    var paciente = {
        nome: form.nome.value,
        peso: form.peso.value,
        gordura: form.gordura.value,
        altura: form.altura.value,
        imc: calculaImc(form.peso.value, form.altura.value)
    };

    return paciente;

}


// funcao para montar as tds
function montaTd(dado, classe) {
    var td = document.createElement("td");
    td.classList.add(classe);
    td.textContent = dado;
    return td;
}


// funcao para montar as trs
function montaTr(paciente) {
    // cria Tr
    var pacienteTr = document.createElement("tr");
    pacienteTr.classList.add("paciente");

// adiciona nas td os itens da td
    pacienteTr.appendChild(montaTd(paciente.nome, "info-nome"));
    pacienteTr.appendChild(montaTd(paciente.peso, "info-peso"));
    pacienteTr.appendChild(montaTd(paciente.altura, "info-altura"));
    pacienteTr.appendChild(montaTd(paciente.gordura, "info-gordura"));
    pacienteTr.appendChild(montaTd(paciente.imc, "info-imc"));

    return pacienteTr;
}

function validaPaciente(paciente) {
    var erros = [];

    if (paciente.nome.length == 0) {
        erros.push("O campo Nome nao pode ser em branco");
    }

    if (paciente.peso.length == 0) {
        erros.push("O campo Peso nao pode ser em branco");
    }
    if (paciente.altura.length == 0) {
        erros.push("O campo Altura nao pode ser em branco");
    }
    if (paciente.gordura.length == 0) {
        erros.push("O campo Gordura nao pode ser em branco");
    }

    if (!validaPeso(paciente.peso)) {
        erros.push("Informe um peso valido");
    }
    if (!validaAltura(paciente.altura)) {
        erros.push("Informe um peso valido");
    }
    return erros;

}

function exibiMensagensDeErro(erros) {
    // seleciona a tag Ul
    var ul = document.querySelector("#mensagens-erro");
    // seleciona o que tem dentro da UL e limpa os campos
    ul.innerHTML = "";

    erros.forEach(function (erro) {
        // cria um elemento li
        var li = document.createElement("li");
        li.textContent = erro;
        ul.appendChild(li);
    });

}


function adicionaPacienteNaTabela(paciente) {
    var pacienteTr = montaTr(paciente);
    var tabela = document.querySelector("#tabela-pacientes");
    // funcao para adicioanr nas tabelas as trs com as tds do paciente
    tabela.appendChild(pacienteTr);
}