var tabela = document.querySelector('table');

tabela.addEventListener('dblclick', function (event) {

    // Somente executará nosso código caso o elemento em que clicamos seja um <td>
    if (event.target.tagName == 'TD') {
        // aciona a animacao que foi realizada no css
        event.target.parentNode.classList.add("fadeOut");


        setTimeout(function () {
            event.target.parentNode.remove();
        }, 250);

    }
});